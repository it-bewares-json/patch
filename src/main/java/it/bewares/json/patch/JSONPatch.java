/**
 *
 * Copyright 2018 Markus Goellnitz.
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation, either version 3 of the License, or (at your option) any
 * later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 */
package it.bewares.json.patch;

import it.bewares.json.patch.exception.JSONOperationResolveException;
import it.bewares.json.tree.JSONArray;
import it.bewares.json.tree.JSONObject;
import it.bewares.json.tree.JSONValue;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;
import lombok.Getter;
import lombok.Setter;

/**
 * A {@code JSONPatch} is a List of single {@code JSONOperation}s which shall be
 * executed on a JSONStructure.
 *
 * JSON Patch refers to the JavaScript Object Notation Patch in its definition
 * by the IETF in RFC 6902.
 *
 * JSON refers to the JavaScript Object Notation Data Interchange Format in its
 * definition by the IETF in RFC 8259.
 *
 * @author Markus Goellnitz
 * @see <a href="https://tools.ietf.org/html/rfc6902">RFC 6902</a>
 * @see <a href="https://tools.ietf.org/html/rfc8259">RFC 8259</a>
 */
public class JSONPatch {

    @Getter
    @Setter
    private List<JSONOperation> operations;

    /**
     * Allocates a {@code JSONPatch} by default without any operations yet.
     */
    public JSONPatch() {
        this.operations = new ArrayList<>();
    }

    /**
     * Allocates a {@code JSONPatch} with the specified Collection of operation.
     *
     * @param operations Collection of operations
     */
    public JSONPatch(Collection<JSONOperation> operations) {
        this();
        this.operations.addAll(operations);
    }

    /**
     * Allocates a {@code JSONPatch} which is represented by the JSON
     * {@code input}.
     *
     * @param input json array to be converted to the corresponding path
     */
    public JSONPatch(JSONArray<JSONObject> input) {
        this();

        for (JSONObject opObj : input) {
            this.addOperation(new JSONOperation(opObj));
        }
    }

    /**
     * Allocates a {@code JSONArray} containing the specified {@code entries}.
     *
     * @param operations JSON data nodes to be contained by the corresponding
     * patch
     */
    public JSONPatch(JSONOperation... operations) {
        this(Arrays.asList(operations));
    }

    /**
     * Addes the specified operation at the end of the patch sequence.
     *
     * @param operation the operation which shall be added
     */
    public void addOperation(JSONOperation operation) {
        this.operations.add(operation);
    }

    /**
     * Execute this patch sequence on the specified {@code JSONValue}.
     *
     * @param json the json value on which this patch shall be executed
     */
    public void executeOn(JSONValue json) throws JSONOperationResolveException, IllegalArgumentException {
        for (JSONOperation operation : this.operations) {
            operation.executeOn(json);
        }
    }

    /**
     * Execute the specified {@code JSONPatch} on a copy of the specified
     * {@code JSONValue} and returns it.
     *
     * @param patch the patch which shall be executed
     * @param input the json value on which the patch shall be executed
     * @return the modfied json
     */
    public static JSONValue execute(JSONPatch patch, JSONValue input) {
        try {
            JSONValue output = (JSONValue) input.clone();
            patch.executeOn(output);
            return output;
        } catch (CloneNotSupportedException | JSONOperationResolveException ex) {
            return input;
        }
    }

    /**
     * Execute the specified {@code JSONOperation} on a copy of the specified
     * {@code JSONValue} and returns it.
     *
     * @param operation the operation which shall be executed
     * @param input the json value on which the patch shall be executed
     * @return the modfied json
     */
    public static JSONValue execute(JSONOperation operation, JSONValue input) {
        try {
            JSONValue output = (JSONValue) input.clone();
            operation.executeOn(output);
            return output;
        } catch (CloneNotSupportedException ex) {
            throw new JSONOperationResolveException(ex);
        }
    }

}
